﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace EstadoTareas
{
    public class coreDTiWare
    {
        #region Variables Globales
        private SqlConnection db = null;
        #endregion

        #region Constructor
        public coreDTiWare()
        {
            db = new SqlConnection(ConfigurationManager.ConnectionStrings["conGlobal"].ConnectionString);
        }
        #endregion

        public DataTable getTareas(string buscar)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("sp_getTareas", db);
                da.SelectCommand.Parameters.AddWithValue("buscar", buscar);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getPorceTareas(int empresa)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter("sp_getPorceTareas", db);
                da.SelectCommand.Parameters.AddWithValue("empresa", empresa);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                db.Open();
                string valor = da.SelectCommand.ExecuteScalar().ToString();
                return valor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }

        public string updateTarea(int id, int empresa, Boolean valor)
        {
            using (SqlCommand cmd = new SqlCommand("sp_updateTarea", db))
            {
                cmd.Parameters.AddWithValue("id", id);
                cmd.Parameters.AddWithValue("empresa", empresa);
                cmd.Parameters.AddWithValue("valor", valor);

                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    db.Open();
                    cmd.ExecuteNonQuery();
                    return "OK";
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    db.Close();
                }
            }
        }

        public void RunScript(string script)
        {

        }

        /*
        CREATE TABLE GP2015_TAREAS(
            tarea nvarchar(250) primary key,
            iav bit,
            corpal bit,
            rectima bit,
            allparts bit,
            depo bit,
            cao bit,
            ptar bit,
            iavec bit
        );

		INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Definir Bodegas',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Crear Bodegas',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Definir Clases Articulos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Crear Clases Articulos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Maestro Clientes',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Maestro Proveedores',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Armar Diccionario',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Generar Codigos Articulos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Maestro Articulos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Maestro Analiticas',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Maestro Activos Fijos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Localizaciones Clientes',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Localizaciones Proveedores',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Config DYMA Clientes',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Config DYMA Proveedores',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Config DYMA Articulos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Ingresar Maestros DYMA Clientes',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Ingresar Maestros DYMA Proveedores',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Ingresar Maestros DYMA Articulos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Creacion de Usuarios',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Roles de Usuarios',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Subir Codigos Anteriores',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Listas de Precios',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Codigos Originales',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Marca Producto',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Plantilla Articulos DYMA',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Plantilla Proveedores DYMA',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Plantilla Clientes DYMA',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Control de Documentos DYMA',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Revisar Saldo de Inventario',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Revisar Saldo de cxc',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Revisar Saldo de cxp',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Revisar Saldo de Bancos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Revisar Compras en Transito',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Revisar Ventas en Transito',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Subir Cupos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Subir Lista de Precios',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Subir Proveedores Articulos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Subir Bodegas Articulos',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Ventas Altiora Ventas',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Ventas Altiora Compras',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Ventas Altiora Financiero',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Ventas Altiora Inventario',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Ventas Altiora Produccion',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Definir Usuario Ventana Ventas',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Definir Usuario Ventana Compras',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Definir Usuario Ventana Financiero',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Definir Usuario Ventana Inventario',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Definir Usuario Ventana Produccion',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Configurar Ventana Ventas',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Configurar Ventana Compras',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Configurar Ventana Inventario',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Configurar Ventana Financiero',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Configurar Ventana Produccion',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Reporte de Retencion',0,0,0,0,0,0,0,0);
        INSERT INTO GP_TAREAS(tarea,IAV,CORPAL,RECTIMA,ALLPARTS,DEPO,CAO,PTAR,IAVEC) VALUES('Reporte de Cheque',0,0,0,0,0,0,0,0);
        */
    }
}
