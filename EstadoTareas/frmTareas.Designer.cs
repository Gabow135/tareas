﻿namespace EstadoTareas
{
    partial class frmTareas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTareas));
            this.gvTareas = new MetroFramework.Controls.MetroGrid();
            this.btnActualizar = new MetroFramework.Controls.MetroButton();
            this.txtBuscar = new MetroFramework.Controls.MetroTextBox();
            this.lblIav = new MetroFramework.Controls.MetroLabel();
            this.lblCorpal = new MetroFramework.Controls.MetroLabel();
            this.tmrPorcentaje = new System.Windows.Forms.Timer(this.components);
            this.lblRectima = new MetroFramework.Controls.MetroLabel();
            this.lblAllparts = new MetroFramework.Controls.MetroLabel();
            this.lblDepo = new MetroFramework.Controls.MetroLabel();
            this.lblCao = new MetroFramework.Controls.MetroLabel();
            this.lblIavec = new MetroFramework.Controls.MetroLabel();
            this.lblPtar = new MetroFramework.Controls.MetroLabel();
            this.lblVihalPinturas = new MetroFramework.Controls.MetroLabel();
            this.lblVihalMotos = new MetroFramework.Controls.MetroLabel();
            this.lblTecnoAsfaltos = new MetroFramework.Controls.MetroLabel();
            this.lblIavGroup = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.gvTareas)).BeginInit();
            this.SuspendLayout();
            // 
            // gvTareas
            // 
            this.gvTareas.AllowUserToAddRows = false;
            this.gvTareas.AllowUserToDeleteRows = false;
            this.gvTareas.AllowUserToResizeRows = false;
            this.gvTareas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gvTareas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gvTareas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gvTareas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvTareas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gvTareas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvTareas.DefaultCellStyle = dataGridViewCellStyle2;
            this.gvTareas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvTareas.EnableHeadersVisualStyles = false;
            this.gvTareas.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.gvTareas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gvTareas.Location = new System.Drawing.Point(20, 60);
            this.gvTareas.Name = "gvTareas";
            this.gvTareas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvTareas.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gvTareas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gvTareas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvTareas.Size = new System.Drawing.Size(1145, 243);
            this.gvTareas.TabIndex = 0;
            this.gvTareas.Theme = MetroFramework.MetroThemeStyle.Light;
            this.gvTareas.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvTareas_CellEndEdit);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Location = new System.Drawing.Point(136, 13);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(115, 29);
            this.btnActualizar.TabIndex = 1;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnActualizar.UseSelectable = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // txtBuscar
            // 
            // 
            // 
            // 
            this.txtBuscar.CustomButton.Image = null;
            this.txtBuscar.CustomButton.Location = new System.Drawing.Point(190, 1);
            this.txtBuscar.CustomButton.Name = "";
            this.txtBuscar.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBuscar.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBuscar.CustomButton.TabIndex = 1;
            this.txtBuscar.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBuscar.CustomButton.UseSelectable = true;
            this.txtBuscar.CustomButton.Visible = false;
            this.txtBuscar.Lines = new string[0];
            this.txtBuscar.Location = new System.Drawing.Point(299, 19);
            this.txtBuscar.MaxLength = 32767;
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.PasswordChar = '\0';
            this.txtBuscar.PromptText = "Buscar Tarea";
            this.txtBuscar.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBuscar.SelectedText = "";
            this.txtBuscar.SelectionLength = 0;
            this.txtBuscar.SelectionStart = 0;
            this.txtBuscar.ShortcutsEnabled = true;
            this.txtBuscar.Size = new System.Drawing.Size(212, 23);
            this.txtBuscar.TabIndex = 2;
            this.txtBuscar.UseSelectable = true;
            this.txtBuscar.WaterMark = "Buscar Tarea";
            this.txtBuscar.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBuscar.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuscar_KeyPress);
            // 
            // lblIav
            // 
            this.lblIav.AutoSize = true;
            this.lblIav.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblIav.Location = new System.Drawing.Point(527, 13);
            this.lblIav.Name = "lblIav";
            this.lblIav.Size = new System.Drawing.Size(24, 15);
            this.lblIav.TabIndex = 3;
            this.lblIav.Text = "Iav:";
            this.lblIav.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblCorpal
            // 
            this.lblCorpal.AutoSize = true;
            this.lblCorpal.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblCorpal.Location = new System.Drawing.Point(527, 36);
            this.lblCorpal.Name = "lblCorpal";
            this.lblCorpal.Size = new System.Drawing.Size(43, 15);
            this.lblCorpal.TabIndex = 4;
            this.lblCorpal.Text = "Corpal:";
            this.lblCorpal.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // tmrPorcentaje
            // 
            this.tmrPorcentaje.Enabled = true;
            this.tmrPorcentaje.Interval = 50000;
            this.tmrPorcentaje.Tick += new System.EventHandler(this.tmrPorcentaje_Tick);
            // 
            // lblRectima
            // 
            this.lblRectima.AutoSize = true;
            this.lblRectima.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblRectima.Location = new System.Drawing.Point(611, 36);
            this.lblRectima.Name = "lblRectima";
            this.lblRectima.Size = new System.Drawing.Size(50, 15);
            this.lblRectima.TabIndex = 6;
            this.lblRectima.Text = "Rectima:";
            this.lblRectima.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblAllparts
            // 
            this.lblAllparts.AutoSize = true;
            this.lblAllparts.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblAllparts.Location = new System.Drawing.Point(611, 13);
            this.lblAllparts.Name = "lblAllparts";
            this.lblAllparts.Size = new System.Drawing.Size(48, 15);
            this.lblAllparts.TabIndex = 5;
            this.lblAllparts.Text = "Allparts:";
            this.lblAllparts.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblDepo
            // 
            this.lblDepo.AutoSize = true;
            this.lblDepo.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblDepo.Location = new System.Drawing.Point(715, 36);
            this.lblDepo.Name = "lblDepo";
            this.lblDepo.Size = new System.Drawing.Size(38, 15);
            this.lblDepo.TabIndex = 8;
            this.lblDepo.Text = "Depo:";
            this.lblDepo.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblCao
            // 
            this.lblCao.AutoSize = true;
            this.lblCao.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblCao.Location = new System.Drawing.Point(715, 13);
            this.lblCao.Name = "lblCao";
            this.lblCao.Size = new System.Drawing.Size(30, 15);
            this.lblCao.TabIndex = 7;
            this.lblCao.Text = "Cao:";
            this.lblCao.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblIavec
            // 
            this.lblIavec.AutoSize = true;
            this.lblIavec.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblIavec.Location = new System.Drawing.Point(803, 36);
            this.lblIavec.Name = "lblIavec";
            this.lblIavec.Size = new System.Drawing.Size(35, 15);
            this.lblIavec.TabIndex = 10;
            this.lblIavec.Text = "Iavec:";
            this.lblIavec.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblPtar
            // 
            this.lblPtar.AutoSize = true;
            this.lblPtar.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblPtar.Location = new System.Drawing.Point(803, 13);
            this.lblPtar.Name = "lblPtar";
            this.lblPtar.Size = new System.Drawing.Size(31, 15);
            this.lblPtar.TabIndex = 9;
            this.lblPtar.Text = "Ptar:";
            this.lblPtar.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblVihalPinturas
            // 
            this.lblVihalPinturas.AutoSize = true;
            this.lblVihalPinturas.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblVihalPinturas.Location = new System.Drawing.Point(892, 36);
            this.lblVihalPinturas.Name = "lblVihalPinturas";
            this.lblVihalPinturas.Size = new System.Drawing.Size(76, 15);
            this.lblVihalPinturas.TabIndex = 12;
            this.lblVihalPinturas.Text = "Vihal Pinturas:";
            this.lblVihalPinturas.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblVihalMotos
            // 
            this.lblVihalMotos.AutoSize = true;
            this.lblVihalMotos.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblVihalMotos.Location = new System.Drawing.Point(892, 13);
            this.lblVihalMotos.Name = "lblVihalMotos";
            this.lblVihalMotos.Size = new System.Drawing.Size(69, 15);
            this.lblVihalMotos.TabIndex = 11;
            this.lblVihalMotos.Text = "Vihal Motos:";
            this.lblVihalMotos.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblTecnoAsfaltos
            // 
            this.lblTecnoAsfaltos.AutoSize = true;
            this.lblTecnoAsfaltos.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblTecnoAsfaltos.Location = new System.Drawing.Point(1025, 36);
            this.lblTecnoAsfaltos.Name = "lblTecnoAsfaltos";
            this.lblTecnoAsfaltos.Size = new System.Drawing.Size(80, 15);
            this.lblTecnoAsfaltos.TabIndex = 14;
            this.lblTecnoAsfaltos.Text = "TecnoAsfaltos:";
            this.lblTecnoAsfaltos.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // lblIavGroup
            // 
            this.lblIavGroup.AutoSize = true;
            this.lblIavGroup.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblIavGroup.Location = new System.Drawing.Point(1025, 13);
            this.lblIavGroup.Name = "lblIavGroup";
            this.lblIavGroup.Size = new System.Drawing.Size(59, 15);
            this.lblIavGroup.TabIndex = 13;
            this.lblIavGroup.Text = "Iav Group:";
            this.lblIavGroup.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // frmTareas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 323);
            this.Controls.Add(this.lblTecnoAsfaltos);
            this.Controls.Add(this.lblIavGroup);
            this.Controls.Add(this.lblVihalPinturas);
            this.Controls.Add(this.lblVihalMotos);
            this.Controls.Add(this.lblIavec);
            this.Controls.Add(this.lblPtar);
            this.Controls.Add(this.lblDepo);
            this.Controls.Add(this.lblCao);
            this.Controls.Add(this.lblRectima);
            this.Controls.Add(this.lblAllparts);
            this.Controls.Add(this.lblCorpal);
            this.Controls.Add(this.lblIav);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.gvTareas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmTareas";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Tareas";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gvTareas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroGrid gvTareas;
        private MetroFramework.Controls.MetroButton btnActualizar;
        private MetroFramework.Controls.MetroTextBox txtBuscar;
        private MetroFramework.Controls.MetroLabel lblIav;
        private MetroFramework.Controls.MetroLabel lblCorpal;
        private System.Windows.Forms.Timer tmrPorcentaje;
        private MetroFramework.Controls.MetroLabel lblRectima;
        private MetroFramework.Controls.MetroLabel lblAllparts;
        private MetroFramework.Controls.MetroLabel lblDepo;
        private MetroFramework.Controls.MetroLabel lblCao;
        private MetroFramework.Controls.MetroLabel lblIavec;
        private MetroFramework.Controls.MetroLabel lblPtar;
        private MetroFramework.Controls.MetroLabel lblVihalPinturas;
        private MetroFramework.Controls.MetroLabel lblVihalMotos;
        private MetroFramework.Controls.MetroLabel lblTecnoAsfaltos;
        private MetroFramework.Controls.MetroLabel lblIavGroup;
    }
}

