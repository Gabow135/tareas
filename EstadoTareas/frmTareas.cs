﻿using System;
using System.Windows.Forms;

namespace EstadoTareas
{
    public partial class frmTareas : MetroFramework.Forms.MetroForm
    {
        #region Variables Globales
        coreDTiWare core = new coreDTiWare();
        #endregion

        public frmTareas()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarGrid("");
            lblIav.Text = "IAV: " + core.getPorceTareas(2) + " %";
            lblCorpal.Text = "Corpal: " + core.getPorceTareas(3) + " %";
            lblRectima.Text = "Rectima: " + core.getPorceTareas(4) + " %";
            lblAllparts.Text = "Allparts: " + core.getPorceTareas(5) + " %";
            lblDepo.Text = "Depo: " + core.getPorceTareas(6) + " %";
            lblCao.Text = "CAO: " + core.getPorceTareas(7) + " %";
            lblPtar.Text = "PTar: " + core.getPorceTareas(8) + " %";
            lblIavec.Text = "Iavec: " + core.getPorceTareas(9) + " %";
            lblVihalMotos.Text = "VihalMotos: " + core.getPorceTareas(10) + " %";
            lblVihalPinturas.Text = "VihalPinturas: " + core.getPorceTareas(11) + " %";
            lblIavGroup.Text = "Iavgroup: " + core.getPorceTareas(12) + " %";
            lblTecnoAsfaltos.Text = "TecnoAsfaltos: " + core.getPorceTareas(13) + " %";
        }
        
        private void gvTareas_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {           
            bool valor = Convert.ToBoolean(gvTareas.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
            int id = Convert.ToInt32(gvTareas.Rows[e.RowIndex].Cells["ID"].Value);            
            core.updateTarea(id, e.ColumnIndex, valor);            
        }

        private void CargarGrid(string buscar)
        {
            gvTareas.DataSource = core.getTareas(buscar);
            gvTareas.Columns[0].ReadOnly = true;
            gvTareas.Columns[1].ReadOnly = true;
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            CargarGrid("");
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            CargarGrid(txtBuscar.Text);
        }

        private void tmrPorcentaje_Tick(object sender, EventArgs e)
        {
            lblIav.Text = "IAV: " + core.getPorceTareas(2) + " %";
            lblCorpal.Text = "Corpal: " + core.getPorceTareas(3) + " %";
            lblRectima.Text = "Rectima: " + core.getPorceTareas(4) + " %";
            lblAllparts.Text = "Allparts: " + core.getPorceTareas(5) + " %";
            lblDepo.Text = "Depo: " + core.getPorceTareas(6) + " %";
            lblCao.Text = "CAO: " + core.getPorceTareas(7) + " %";
            lblPtar.Text = "PTar: " + core.getPorceTareas(8) + " %";
            lblIavec.Text = "Iavec: " + core.getPorceTareas(9) + " %";
            lblVihalMotos.Text = "VihalMotos: " + core.getPorceTareas(10) + " %";
            lblVihalPinturas.Text = "VihalPinturas: " + core.getPorceTareas(11) + " %";
            lblIavGroup.Text = "Iavgroup: " + core.getPorceTareas(12) + " %";
            lblTecnoAsfaltos.Text = "TecnoAsfaltos: " + core.getPorceTareas(13) + " %";
        }
    }
}
